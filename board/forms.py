from django import forms


class BoardForm(forms.Form):
    # 템플릿에서 데이터 받을 각각 필드
    title = forms.CharField(
        error_messages={'required': '제목을 입력하세요!'},
        max_length=128, label='제목')
    contents = forms.CharField(
        error_messages={'required': '내용을 입력하세요!'},
        widget=forms.Textarea, label='내용')
    tags = forms.CharField(
        widget=False, label='태그'
    )