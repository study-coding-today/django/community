from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.http import Http404
from .models import Board
from user.models import User
from .forms import BoardForm
from tag.models import Tag


# 게시글을 출력하는 함수
def board_list(request):
    # 모델에 있는 필드들의 값을 아이디 내림차순으로 가져온다.
    all_boards = Board.objects.all().order_by('-id')
    # GET 방식으로 페이지 번호을 p 변수에 받는데 없으면 첫 페이지로 받는다.
    page = int(request.GET.get('p', 1))
    # 페이지네이터 메서드에 받은 객체를 모두 넣는데 2개씩 보여준다.
    paginator = Paginator(all_boards, 2)
    boards = paginator.get_page(page)
    # 가져온 데이터를 템플릿에 넘겨주고 랜더링 한다.
    return render(request, 'board/board_list.html', {'boards': boards})


# 글쓰기 함수
def board_write(request):
    # 글쓰기전 세션에 로그인 되었는지 확인한다. 로그인 전이면 로그인페이지로 넘긴다.
    if not request.session.get('user'):
        return redirect('/user/login/')
    # 요청한 방식이 post 인지 확인하여
    if request.method == 'POST':
        # 요청한 데이터로 폼 인스턴스를 생성하고
        form = BoardForm(request.POST)
        # 데이터가 모두 있는지 유효성 검사를 한뒤
        if form.is_valid():
            # 로그인된 사용자가 글을 쓸 수 있으므로 세션에서 유저 id를 가져온다.
            user_id = request.session.get('user')
            # User 의 모델를 가져오기 위해 세션 아이디를 넣어 가져온다.
            user = User.objects.get(pk=user_id)
            # 모델 인스턴스를 생성하여 각 필드에 폼에서 가져오 데이터로 할당한다.
            board = Board()
            board.title = form.cleaned_data['title']
            board.contents = form.cleaned_data['contents']
            board.writer = user
            board.save()

            # Form 으로 가져온 데이터 tag 가 이성없으면 , 로 구분하여 할당
            tags = form.cleaned_data['tags'].split(',')

            for tag in tags:
                if not tag:
                    continue
                # 가져온 태그가 name 과 이름이 같으면 가죠와 _tag 에 할당 없으면 태그객체 생성
                # 새로생성된 여부가 created 에 boolean 값으로 들어간다.
                # 하지마 여기서는 새로 만들어지나 가죠오가 관계없어 created 를 _로 생략한다.
                # _tag, created = Tag.objects.get_or_create(name=tag)
                _tag, _ = Tag.objects.get_or_create(name=tag)
                # 생성된 객체를 board 의 태그에 할당한다.
                board.tags.add(_tag)

            return redirect('/board/list/')
    else:
        form = BoardForm()

    return render(request, 'board/board_write.html', {'form': form})


# 게시글 상세보기 함수로 urls 로부터 게시글 id를 가져온다.
def board_detail(request, pk):
    try:
        # 가져온 게시글의 id로 모델의 해당 id를 가져와 할당하고 템플릿에 랜더링하며 넘겨준다.
        board = Board.objects.get(pk=pk)
    except Board.DoesNotExist:  # 해당하는 게시글이 없을 때
        raise Http404('게시글을 찾을 수 없습니다.')

    return render(request, 'board/board_detail.html', {'board': board})
