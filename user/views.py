from django.shortcuts import render, redirect
from .models import User
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password, check_password
from .forms import LoginForm


# 메인 페이지 뷰
def home(request):
    # 세션에 있는 user 의 id를 가져온다.
    user_id = request.session.get('user')
    if user_id:
        # User 모델과 세션 user_id 가 같은 사용자 데이터를 모두 가져오기
        user = User.objects.get(pk=user_id)
        # 가져오 모델의 username 을 메인페이지에 출력하기
        return render(request, 'home.html', {'user': user})
    # 로그인 되어 있지 않으면 username 이 없이 메인페이지만 출력
    return render(request, 'home.html')


def logout(request):
    if request.session.get('user'):
        del(request.session['user'])

    return redirect('/')


# 로그인 함수
def login(request):
    if request.method == 'POST':
        # request 로 받은 POST 로 폼 클래스를 가져와 폼 인스턴스를 생성한다.
        # Form 객체의 주요 작업은 데이터의 유효성 검사를 하는 것이다.
        form = LoginForm(request.POST)
        # 받은 form 의 유효성 검사를 한다. 기본적으로 모든 필드가 필수이다.
        # 모든 필드가 유효하면 true 를 반환한다.
        # 필드가 유효하지 않으면 Form.errors 안에 사전형식의 error 메시지를 가져올 수 있다.
        # 이 딕션너리에 key 는 필드이고 value 는 오류 메시지 문자열 이다.
        if form.is_valid():

            request.session['user'] = form.user_id
            return redirect('/')
    else:
        form = LoginForm()
    # 폼 인스턴스를 로그인 페이지에 딕셔러리 타입으로 보내면서 랜더링한다.
    return render(request, 'user/login.html', {'form': form})

    # # 로그인 페이지 요청 방식이 GET 방식이면
    # if request.method == 'GET':
    #     # 그대로 로그인 페이지 렌더링
    #     return render(request, 'user/login.html')
    # # 로그인 페이지 요청 방식이 POST 방식이면
    # if request.method == 'POST':
    #     # username 과 비밀번호를 가져온다.
    #     username = request.POST.get('username', None)
    #     password = request.POST.get('password', None)
    #     # 로그인 항목이 비어 있을 때 넣어줄 error 변수
    #     res_data = {}
    #     # username 과 페스워드가 없다면
    #     if not (username and password):
    #         # error 키에 메시지를 담는다.
    #         res_data['error'] = '모든 값을 입력하세요'
    #     # User 모델에 username 과 로그인 페이지에서 가져오 username 이 같은 모델의 데이터 모두 가져오기
    #     user = User.objects.get(username=username)
    #     # 로그인 페이지의 비밀번호와 모델에거 가져온 비빌번호가 같은지 확인
    #     if check_password(password, user.password):
    #         # 서버의 세션에 user 키로 user.id를 넣는다
    #         request.session['user'] = user.id
    #         # 로그인 되면 메인페이지로 넘어간다.
    #         return redirect('/')
    #     else:
    #         # 비밀번호가 틀렸으면 error 메시지를 대입한다.
    #         res_data['error'] = '비밀번호가 틀렸습니다.'
    #     # 에러 메시지를 갖고 로그인 페이지로 랜더링 한다.
    #     return render(request, 'user/login.html', res_data)


# 회원가입 함수
def register(request):
    # 회원가입 페이지 요청 방식이 GET 방식이면
    if request.method == 'GET':
        return render(request, 'user/register.html')
    # 회원가입 페이지 요청 방식이 POST 방식이면
    if request.method == 'POST':
        username = request.POST.get('username', None)
        useremail = request.POST.get('useremail', None)
        password = request.POST.get('password', None)
        re_password = request.POST.get('re_password', None)

        res_data = {}
        if not (username and useremail and password and re_password):
            res_data['error'] = '모든 값을 입력하세요.'
        if password != re_password:
            res_data['error'] = '비밀번호가 다릅니다.'

        # user 인스턴스 생성
        user = User(
            username=username,
            useremail=useremail,
            password=make_password(password)
        )
        # 디비에 저장
        user.save()

        return render(request, 'user/register.html', res_data)
