from django.urls import path
from . import views

urlpatterns = [
    # 회원가입 url 로 확인시 views 에 register 함수 호출
    path('register/', views.register),
    # 로그인 url 로 확인시 views 에 login 함수 호출
    path('login/', views.login),
    # 로그아웃
    path('logout/', views.logout),
]