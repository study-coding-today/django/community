from django.contrib import admin
from .models import User


class UserAdmin(admin.ModelAdmin):
    # admin 페이지에 목록 표시하기
    list_display = ['username', 'useremail', 'password']
    

admin.site.register(User, UserAdmin)



