from django import forms
from .models import User
from django.contrib.auth.hashers import check_password


# 템플릿에 로그인 데이터를 받을 form 클래스
class LoginForm(forms.Form):
    # 템플릿에서 데이터 받을 각각 필드
    username = forms.CharField(
        error_messages={'required': '아이디를 입력하세요!'},
        max_length=32, label='사용자')
    password = forms.CharField(
        error_messages={'required': '비밀번호를 입력하세요!'},
        widget=forms.PasswordInput, label='비밀번호')

    def clean(self):
        # super()에 의해 Form.clean()의 부모가 갖고 있는 유효성검사된 데이터를 가져와 할당
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        if username and password:
            # 사용자가 없을 경우 예외 처리
            try:
                # 모델의 username 과 form 으로 부터 가져온 username 이 같은 data 를 가져온다.
                user = User.objects.get(username=username)
            except User.DoesNotExist:   # 모델에 같은 사용자가 없을 경우
                # error 에 error 메시지 넣는다.
                self.add_error('username', '아이디가 없습니다.')
                return
            # form 에서 받아온 비밀번호와 모델의 비밀번호가 같은지 확인
            if not check_password(password, user.password):
                # 비밀번호가 다르면 Form 안에 add_error 메서드로 password key 에 에러메시지를 넣는다.
                self.add_error('password', '비밀번호가 틀렸습니다.')
            else:
                # 비밀번호가 같으면 이 클래스에 user_id를 넣고 id를 할당한다. 외부에서 접근할 수 있다.
                self.user_id = user.id