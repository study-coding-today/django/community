from django.db import models


class User(models.Model):
    username = models.CharField(max_length=64, verbose_name='사용자')
    useremail = models.EmailField(max_length=128, verbose_name='이메일')
    password = models.CharField(max_length=64, verbose_name='비밀번호')
    registered_date = models.DateTimeField(auto_now_add=True, verbose_name='등록시간')

    def __str__(self):          # admin 페이지 username 으로 출력
        return self.username    # admin.py 에서 list_display 로 필드로 설정해도 출력됨

    class Meta:
        db_table = 'USER'       # 테이블명을 USER 로 지정
        verbose_name = '사용자'  # admin 페이지 USER 테이블의 항목을 한글로 표시
        verbose_name_plural = '사용자'  # 위의 verbose_name 과 함께 작성한다.
